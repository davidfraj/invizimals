//////////////////////////////////////////////////////////////////////////////
//COMIENZO DE: Funcion para generar una carta segun colores, pasandole vector
function gen_carta_vec(vector){
	//Declaro la variable que devolvere
	var carta="";

	//Relleno la variable con los colores del vector
	carta='<div class="carta">';
	for (var i = 0; i < vector.length; i++) {
		carta=carta+'<div class="bloque '+vector[i]+'">&nbsp;</div>';
	}
	carta=carta+'</div>';

	//devuelvo la carta
	return carta;
}
//FIN DE: Funcion para generar una carta segun colores, pasandole vector
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//COMIENZO DE: Funcion para generar una carta segun colores, pasandole cadena
function gen_carta_cad(cadena){
	//Declaro la variable que devolvere
	var carta="";

	//Paso la cadena a minusculas por si acaso
	cadena=cadena.toLowerCase();

	//Relleno la variable con los colores del vector
	carta='<div class="carta">';
	for (var i = 0; i < cadena.length; i++) {
		carta=carta+'<div class="bloque '+cadena.substr(i,1)+'">&nbsp;</div>';
	}
	carta=carta+'</div>';

	//devuelvo la carta
	return carta;
}
//FIN DE: Funcion para generar una carta segun colores, pasandole cadena
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//COMIENZO DE: Funcion para cambiar la carta, pasandole un numero, y una posicion
function cambiarCarta(numero,id){
	document.getElementById(id).innerHTML=gen_carta_cad(cartas[numero]);
}
//FIN DE: Funcion para cambiar la carta, pasandole un numero, y una posicion
//////////////////////////////////////////////////////////////////////////////